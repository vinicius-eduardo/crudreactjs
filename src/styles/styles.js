import styled, { createGlobalStyle } from 'styled-components';

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  h1 {
    padding: 1rem 0;
  }

  #search-form {
    flex-direction: row;
    margin-top: 1rem;
  }
  #search-form input {
    width: 25rem;
    padding: .8rem;
    border-radius: 20px;
  }
`;

export const Forms = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  input {
    width: 12rem;
    padding: 1rem;
    margin: .3rem;
    border-radius: 3px;
    border: 1px solid blue;
  }
`;

export const ErrMessage = styled.p`
  color: red;
  padding: 5px 0;
  font-size: 14px;
`;

export const Button = styled.button`
  width: 5rem;
  cursor: pointer;
  border-radius: 25px;
  color: white;
  background: blue;
  border: none;
  margin: .3rem;
  padding: .6rem;
`;

export const Cards = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  grid-gap: 2.5rem;
  margin-top: 2rem;
`;

export const Card = styled.div`
  padding: 1rem;
  border-radius: 5px;
  border: 1px solid violet;
  background: violet;

  h2 {
    padding: .5rem 0;
  }

  p {
    padding: .5rem 0;
  }
`;

export const CardButtons = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;



export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }
  body {
    font-family: arial;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    background: aqua;
  }
`;