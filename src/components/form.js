import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Header, Button, Forms, Cards, Card, CardButtons, ErrMessage } from '../styles/styles';
import SearchForm from './searchForm';
import api from '../services/api';

function Form () {
  const [cards, setCards] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [cardOnEdit, setCardOnEdit] = useState();

  const { register, handleSubmit, errors, reset } = useForm();
  
  async function updateCurrentCards () {
    try {
      const response = await api.get('/notes');

      setCards(response.data);

    } catch (err) {
      global.console.log(err);
    }
  };

  useEffect(() => {
    updateCurrentCards();
  }, []);

  const onSubmit = async (data) => {

    try {
      await api.post('/notes', data);
    } catch (err) {
      global.console.log(err);
    }

    updateCurrentCards();

    reset();
  }

  const getCard = (id) => {
    const onEdit = cards.find( (card) => card._id === id );
    
    setCardOnEdit(onEdit);

    setIsEditing(true);
  }

  const removeCard = async (id) => {

    try {
      await api.delete(`/notes/${id}`);
    } catch (err) {
      global.console.log(err);
    }

    updateCurrentCards();

  }

  const editInfo = async (data) => {

    try {
      await api.put(`/notes/${cardOnEdit._id}`, data);
    } catch (err) {
      global.console.log(err);
    }

    updateCurrentCards();

    setIsEditing(false);

    reset();
  }

  return (
    <>
      <Header>
        <h1>Criar nota</h1>
        <Forms onSubmit={handleSubmit(onSubmit)}>

          {errors.title && <ErrMessage>*preencha o titulo</ErrMessage>}
          <input type="text" 
          placeholder="Titulo" 
          defaultValue="" 
          name="title" 
          ref={ register({required: true}) }/>

          {errors.content && <ErrMessage>*preencha a nota</ErrMessage>}
          <input type="text" 
          placeholder="Nota" 
          defaultValue="" 
          name="content" 
          ref={ register({required: true}) }/>

          <Button type="submit">Criar</Button>
        </Forms>

        <SearchForm updateCurrentCards={updateCurrentCards} setCards={setCards} />
        
      </Header>
      
      <Cards>
      { 
        cards.map( (card) => {
          const { _id, title, content } = card;

          return (
            
            <Card key={_id}>
              { 
                isEditing && cardOnEdit._id === _id ? (
                  <>
                    <h1>Ediar nota</h1>
                    <Forms onSubmit={handleSubmit(editInfo)}>
                      
                      {errors.title && <ErrMessage>*preencha o titulo</ErrMessage>}
                      <input type="text" 
                      placeholder={title} 
                      defaultValue="" 
                      name="title" 
                      ref={ register( {required: true }) }/>

                      {errors.content && <ErrMessage>*preencha a nota</ErrMessage>}
                      <input type="text" 
                      placeholder={content} 
                      defaultValue="" 
                      name="content" 
                      ref={ register({required: true}) }/>
                      
                      <CardButtons>
                        <Button type="submit">Salvar</Button>
                        <Button onClick={() => setIsEditing(false)}>Cancelar</Button>
                      </CardButtons>
                      
                    </Forms>
                  </>
                ) : (
                  <>
                    <h2>{title}</h2>
                    <p>{content}</p>
                    <CardButtons>
                      <Button onClick={() => getCard(_id)}>Editar</Button>
                      <Button onClick={() => removeCard(_id)}>Deletar</Button>
                    </CardButtons>
                  </>
                )
              }
            </Card>

          );
        })
      }
      </Cards>
    </>
  );
}

export default Form;