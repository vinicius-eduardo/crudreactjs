import React from 'react';
import { useForm } from 'react-hook-form';
import { Forms, Button } from '../styles/styles';

import api from '../services/api';

function SearchForm (props) {
  
  const { register, handleSubmit, reset } = useForm();

  const searchCard = async (data) => {

    try {
      const response = await api.get(`/notes/${data.search}`);
      
      props.setCards([response.data]);
      
      reset();
    } catch (err) {
      global.console.log(err.response.data);
    }

  }

  const handleClear = () => {
    props.updateCurrentCards();
  }

  return (
    <>
      <Forms id="search-form" onSubmit={handleSubmit(searchCard)}>
        <input type="text" 
        placeholder="Buscar..." 
        name="search" 
        ref={ register({ required: true }) }/>

        <Button type="submit">Buscar</Button>
        <Button onClick={handleClear}>Limpar</Button>
      </Forms>
    </>
  );
}

export default SearchForm;