import GlobalStyle from './styles/styles';
import Form from './components/form';

function App() {
  return (
    <div>
      <GlobalStyle />
      <Form />
    </div>
  );
}

export default App;
