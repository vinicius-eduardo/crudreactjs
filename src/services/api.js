import axios from 'axios';

const api = axios.create({
  baseURL: 'https://node-easy-notes-test.herokuapp.com',
});

export default api;